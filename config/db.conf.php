<?php

declare(strict_types=1);

if (isset($_SERVER['SERVER_PORT']) && (int)$_SERVER['SERVER_PORT'] === 3000) {
    $dbConfig = [
        'db.host' => $_ENV['DB_HOST'] ?? 'dbs',
        'db.name' => $_ENV['DB_NAME'] ?? 'project',
        'db.port' => $_ENV['DB_PORT'] ?? '3306',
        'db.username' => $_ENV['DB_USER'] ?? 'root',
        'db.password' => $_ENV['DB_PASSWORD'] ?? 'root',
        'db.debug' => $_ENV['ENVIRONMENT'] ?? true,
    ];
} else {
    $dbConfig = [
        'db.host' => '127.0.0.1',
        'db.name' => 'project',
        'db.port' => '6604',
        'db.username' => 'root',
        'db.password' => 'root',
        'db.debug' => true,
    ];
}

return $dbConfig;
