#!/usr/bin/env bash

set -o allexport
set +o allexport
cron start -f && tail -f /var/log/cron.log
service nginx start
php-fpm
