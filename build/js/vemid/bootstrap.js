Vemid.language.init();
Vemid.validation.init();
Vemid.form.init();
Vemid.datetime.init();
Vemid.misc.init();
Vemid.uploader.init();
Vemid.notification.init();