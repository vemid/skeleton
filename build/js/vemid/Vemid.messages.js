(function ($) {

    let Vemid = this;

    Vemid.language.add_translation({
        requiredField: {
            en: "Required field!",
            sr: "Obavezno polje!"
        },
        mondayShort: {
            en: "Mon",
            sr: "Pon"
        },
        mondayLong: {
            en: "Monday",
            sr: "Ponedeljak"
        },
        tuesdayShort: {
            en: "Tue",
            sr: "Uto"
        },
        tuesdayLong: {
            en: "Tuesday",
            sr: "Utorak"
        },
        wednesdayShort: {
            en: "Wen",
            sr: "Sre"
        },
        wednesdayLong: {
            en: "Wednesday",
            sr: "Sreda"
        },
        thursdayShort: {
            en: "Thu",
            sr: "Čet"
        },
        thursdayLong: {
            en: "Thuesday",
            sr: "Četvrtak"
        },
        fridayShort: {
            en: "Fri",
            sr: "Pet"
        },
        fridayLong: {
            en: "Friday",
            sr: "Petak"
        },
        saturdayShort: {
            en: "Sat",
            sr: "Sub"
        },
        saturdayLong: {
            en: "Saturday",
            sr: "Subota"
        },
        sundayShort: {
            en: "Sun",
            sr: "Ned"
        },
        sundayLong: {
            en: "Sunday",
            sr: "Nedelja"
        },
        janShort: {
            en: "Jan",
            sr: "Jan"
        },
        janLong: {
            en: "January",
            sr: "Januar"
        },
        febShort: {
            en: "Feb",
            sr: "Feb"
        },
        febLong: {
            en: "February",
            sr: "Februar"
        },
        marShort: {
            en: "Mar",
            sr: "Mar"
        },
        marLong: {
            en: "March",
            sr: "Mart"
        },
        aprShort: {
            en: "Apr",
            sr: "Apr"
        },
        aprLong: {
            en: "April",
            sr: "April"
        },
        mayShort: {
            en: "May",
            sr: "Maj"
        },
        mayLong: {
            en: "May",
            sr: "Maj"
        },
        junShort: {
            en: "Jun",
            sr: "Jun"
        },
        junLong: {
            en: "June",
            sr: "Jun"
        },
        julShort: {
            en: "Jul",
            sr: "Jul"
        },
        julLong: {
            en: "July",
            sr: "Jul"
        },
        augShort: {
            en: "Aug",
            sr: "Avg"
        },
        augLong: {
            en: "August",
            sr: "Avgust"
        },
        sepShort: {
            en: "Sep",
            sr: "Sep"
        },
        sepLong: {
            en: "September",
            sr: "Septembar"
        },
        octShort: {
            en: "Oct",
            sr: "Okt"
        },
        octLong: {
            en: "October",
            sr: "Oktobar"
        },
        novShort: {
            en: "Nov",
            sr: "Nov"
        },
        novLong: {
            en: "November",
            sr: "Novembar"
        },
        decShort: {
            en: "Dec",
            sr: "Dec"
        },
        decLong: {
            en: "December",
            sr: "Decembar"
        },
        choose: {
            en: "-- Choose --",
            sr: "-- Izaberite --"
        },

        chooseFile: {
            en: "Choose",
            sr: "Izaberite"
        },
        areYouSure: {
            en: "Are you sure?",
            sr: "Da li ste sigurni?"
        },
        giveUp: {
            en: "Reject",
            sr: "Odustani"
        },
        delete: {
            en: "Delete",
            sr: "Obriši"
        },
        permissionDenied: {
            en: "Permission denied",
            sr: "Nemate pristupa stranici"
        }

    });
}).call(Vemid, $);